from datetime import datetime
import matplotlib.pyplot as plt
import os
import random

# TODO: Change relevant numbers from hardcoded to passed to the function
# TODO: Decide what to do with the raw data and how to save it


def create_output_dir():
    now = datetime.now()
    output_path = f"output/{now.strftime('%Y%m%d_%H%M%S')}"

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    return output_path


def generate_graphs(graph_config):  # cycle, numbers, names, rand_low, rand_high):
    totals = {}

    for num in range(graph_config['rand_low'], graph_config['rand_high'] + 1):
        totals[num] = graph_config['numbers'].count(num)

    plt.figure(figsize=(6, 4))
    plt.plot(list(totals.keys()), list(totals.values()))
    plt.xlabel('Random number generated')
    plt.ylabel('Total samples = 10,000,000')
    plt.tight_layout()
    plt.savefig(f"{graph_config['output_path']}/{graph_config['graph_name']}_{graph_config['cycle']}.png")
    print(f"Saved graph: {graph_config['output_path']}/{graph_config['graph_name']}_{graph_config['cycle']}")
    plt.close()


def coin_toss(initial_config):
    for cycle in range(1, initial_config['iterations'] + 1):
        random.seed()

        numbers = []

        for step in range(1, 10_000_000):
            numbers.append(random.randint(initial_config['rand_low'], initial_config['rand_high']))

        graph_config = initial_config.copy()
        graph_config['numbers'] = numbers
        graph_config['cycle'] = cycle
        graph_config['graph_name'] = 'coin_toss'
        generate_graphs(graph_config)


# def int_one(cycles=1):
#     for cycle in range(1, cycles + 1):
#         random.seed()
#
#         numbers = []
#
#         for step in range(1, 10_000_000):
#             numbers.append(random.randint(1, 99))
#
#         generate_graphs(cycle, numbers, 'int_one')


# def int_two(cycles=1):
#     for cycle in range(1, cycles + 1):
#         numbers = []
#
#         for sequence in range(1, 1_000):
#             random.seed()
#             for step in range(1, 10_000):
#                 numbers.append(random.randint(1, 99))
#
#         totals = {}
#
#         for num in range(1, 100):
#             totals[num] = numbers.count(num)
#
#         generate_graphs(cycle, numbers, 'int_two')


# def int_three(cycles=1):
#     for cycle in range(1, cycles + 1):
#         numbers = []
#
#         for sequence in range(1, 1_000):
#             random.seed(42)
#             for step in range(1, 10_000):
#                 numbers.append(random.randint(1, 99))
#
#         generate_graphs(cycle, numbers, 'int_three')


# def int_four(cycles=1):
#     for cycle in range(1, cycles + 1):
#         numbers = []
#
#         for step in range(1, 10_000_000):
#             random.seed(42)
#             numbers.append(random.randint(1, 99))
#
#         generate_graphs(cycle, numbers, 'int_four')


def run_all():
    output_path = create_output_dir()
    initial_config = {
        'output_path': output_path,
        'iterations': 10,
        'rand_low': 1,
        'rand_high': 99,
    }
    coin_toss(initial_config)
    # int_one(initial_config)
    # int_two(initial_config)
    # int_three(initial_config)
    # int_four(initial_config)


if __name__ == '__main__':
    run_all()
